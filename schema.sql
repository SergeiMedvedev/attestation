-- создание таблицы
create table product (
                         id serial primary key,
                         product_name varchar(40),
                         price_num varchar(20),
                         age integer check (age > 0 and age < 120)
);
-- добавление информации
insert into product(product_name, price_num, age) values ('Молоко', '55', 27);
insert into product(product_name, price_num, age) values ('Хлеб', '25', 32);
insert into product(product_name, price_num, age) values ('Колбаса', '345', 13);
insert into product(product_name, price_num, age) values ('Яйцо', '75', 17);
insert into product(product_name, price_num, age) values ('Сахар', '55', 33);

-- создание таблицы
create table client(
                       id serial primary key,
                       first_name varchar(20),
                       last_name varchar(20),
                       age integer check (age > 0 and age < 120)
);
-- добавление информации
insert into client(first_name, last_name, age) values ('Марсель', 'Сидиков', 27);
insert into client(first_name, last_name, age) values ('Виктор', 'Евлампьев', 25);
insert into client(first_name, last_name, age) values ('Максим', 'Поздеев', 22);
insert into client(first_name, last_name, age) values ('Разиль', 'Миниахметов', 24);
insert into client(first_name, last_name, age) values ('Тимур', 'Хафизов', 24);

-- создание таблицы
create table orders(
                       id serial primary key,
                       data varchar,
                       number_sum varchar(20),
                       owner_id integer,
                       foreign key(owner_id) references product(id),
                       owner_client_id integer,
                       foreign key(owner_client_id) references client(id)
);

-- создание заказа
insert into orders(data, number_sum, owner_id, owner_client_id) values ('11.11.2021', '2', '4', 5);
insert into orders(data, number_sum, owner_id, owner_client_id) values ('12.11.2021', '3', '2', 4);
insert into orders(data, number_sum, owner_id, owner_client_id) values ('12.11.2021', '4', '2', 2);
insert into orders(data, number_sum, owner_id, owner_client_id) values ('13.11.2021', '2', '3', 2);
insert into orders(data, number_sum, owner_id, owner_client_id) values ('14.11.2021', '2', '4', 3);
insert into orders(data, number_sum, owner_id, owner_client_id) values ('15.11.2021', '4', '5', 3);

-- запросы
---- поличить имя пользователя с id 4 и количество его заказов
select first_name, (select count(*) from orders where owner_id = 4) as order_count
from client where id = 4;

---- получить имена пользователей, которые сделали заказ 12.11.2021
select first_name from client where client.id in
                                    (select owner_id from orders where data = '12.11.2021');

---- попадают все клиенты, и все заказы
select * from client a full join orders c on a.id = c.owner_id;
